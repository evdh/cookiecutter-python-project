Welcome to {{cookiecutter.project_name}}'s documentation!
=========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   
   modules.rst
   glossary.rst

   about.rst
   bugs.rst
   license.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`