#!/usr/bin/env python3
"""
Module {{cookiecutter.python_main_script}}
"""

import argparse
import logging

__author__ = "{{cookiecutter.code_authors}}"
__codeversion__ = "{{cookiecutter.code_version}}"
__license__ = "{{cookiecutter.code_license}}"
__prog__ = f"{{cookiecutter.python_main_script}} {__codeversion__}"
__description__ = "{{cookiecutter.project_description}}"


def main():
    """Main entry point of the app"""
    logger.info("{{cookiecutter.python_main_script}} started")
    print("hello world")
    logger.info("{{cookiecutter.python_main_script}} stopped")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog=__prog__, description=__description__)

    # argument for loglevel handling
    parser.add_argument(
        "--loglevel",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        default="INFO",
        help="Default log level",
    )

    # add your arguments here

    # create the args
    args = parser.parse_args()

    # logger configuration
    LOGFILEMODE = "a"
    LOGFORMAT = "%(asctime)s %(levelname)s %(message)s"
    logger = logging.getLogger(__name__)
    if args.loglevel == "DEBUG":
        LOGLEVEL = logging.DEBUG
        LOGFORMAT = "%(asctime)s %(levelname)s %(filename)s %(funcName)s \
                     %(message)s"
    elif args.loglevel == "INFO":
        LOGLEVEL = logging.INFO
    elif argparse.loglevel == "WARNING":
        LOGLEVEL = logging.WARNING
    elif argparse.loglevel == "ERROR":
        LOGLEVEL = logging.ERROR
    else:
        LOGLEVEL = logging.CRITICAL

    logging.basicConfig(
        filename="{{cookiecutter.python_logname}}.log",
        level=LOGLEVEL,
        filemode=LOGFILEMODE,
        format=LOGFORMAT,
        encoding="utf-8",
    )

    main()
