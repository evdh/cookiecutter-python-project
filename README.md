# cookiecutter-python-project

This repository is a [cookiecutter](https://www.cookiecutter.io/) template for [Python](https://www.python.org/) projects.
The idea is to have a template in order to develop python code and be able to run the code in a Dockerized environment, 
compile it to a distributable python package or as a CLI tool.

The reason for this choice is that it depends on the project, and I want to maintain only one template.

# How to use this repository

## Installing cookiecutter

```bash
pip install cookiecutter
```

### Upgrading cookiecutter

In case you already have an installation, make sure it is the latest. You can upgrade with

```bash
pip install --upgrade cookiecutter
```

## Using this cookiecutter template

```bash
cookiecutter https://gitlab.com/evdh/cookiecutter-python-project.git
```
You will be presented with the wizard to answer the variables in cookiecutter.json. If you just want to give it a go to check everything out, we pre-populated the `cookiecutter.json` file with default values, so hit enter for each question and you will obtain a `MyProject` demo.

## Your git repo
The result of a cookiecutter is just a directory structure no more, no less. This is why there is a need to set up a number of things. The first
thing is of course your local git repository. Go into your project directory and run

```bash
git init
```

This will setup your git repo environment, and thus create the `.git` directory in your project directory. If you want to set your user name specific for that repository (and not use the global one) do

```bash
git config --local user.name <your user name>
```
```bash
git config --local user.email <your email address>
```

We recommended to sign your work, it is just a matter of configuring git. For this, you need a GPG key and then configure your git user to use it.
To configure your user with its key you do

```bash
git config --local user.signingkey <keyid>
```

You can find your `keyid` with 

```bash
gpg --list-keys
```

this outputs something like:

```shell
pub   rsa3072 2024-03-09 [SC] [expires: 2026-03-09]
      4C9D6E1DD11D75A911B7EE6B8A3DFBE648D36591
uid           [ultimate] Erik Vanderhasselt <erik@theshell.company>
sub   rsa3072 2024-03-09 [E] [expires: 2026-03-09]
```

The `keyid` you are looking for is the one of your public key. In the example above the `keyid` is `4C9D6E1DD11D75A911B7EE6B8A3DFBE648D36591`. More information can be found on the [git website](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work).

To sign your commits you do
```bash
git commit -S -m <your commit message>
```

You will be prompted for the password when you sign, to proof your identity.

To configure the repository to make signing commits mandatory you run

```bash
git config --local commit.gpgsign true
```

When working on a MacOS system you might have noticed you have the .DS_Store directory presented to you. Rather than excluding it in the .gitignore of the project you can exclude it at a system level so you don't have to configure it for any other git versioned projects. 

```bash
echo .DS_Store >> ~/.gitignore_global
git config --global core.excludesfile ~/.gitignore_global
```

If for some reason you need it again in your versioning system, it is just a matter of adapting the `.gitignore_global` file in your home directory and running the last line again.

## The virtual environment

Go into your project directory and create a python virtual environment. The idea is to have isolation of your dependencies from the rest of the system.

```bash
python3 -m venv .venv
```

### Starting your virtual environment

```bash
source .venv/bin/activate
```

This starts the virtual environment for your project. You will see at the beginning of your prompt (.venv) indicating the environment is active.

### Quitting the virtual environment

To quit the virtual environment you do

```bash
deactivate
```

This will deactivate the virtual environment for your project. You will see ath the beginning of your prompt that the (.venv) is gone.

# Installation of dependencies

The installation of dependencies is done when the virtual environment is active. This means that you should see the (.venv) at the beginning of your prompt.

## Upgrade of pip

```bash
pip install --upgrade pip
```

This is the python package manager, it is important to keep it up to date.

## Update pyproject for your dependencies

You need to add your dependencies to your `pyproject.toml` file:

```text
  1 [project]                                                                   
  2 name = "<your project>"
  3 version = "<version>"
  4 dependencies = ["dependency 1"," dependency 2"]
```

The idea is that you fill in the dependencies and that list can be generated from the project directory with

```shell
pip freeze
```

## Installing the dependencies

The dependencies are easily installed from `pyproject.toml` by running from the project directory

```bash
pip install -e .
```

## Getting an overview of the installed dependencies

If you are in doubt if the dependencies were installed into your virtual environment you can always run

```bash
pip list
```

## Setting up your development environment dependencies

We set up the development environment dependencies by configuring in `pyproject.toml` the section named `project.optional-dependencies`. To install the dependencies you run from the project directory. That looks like this

```text
[project.optional-dependencies]
dev =  ["build","black", "sphinx", "flake8", "pre-commit", "pylint", "isort", "radon"]
```

Where dev contains the projects you want to help you develop your scripts


## Installing the optional dependencies

To install the optional dependencies you run from the project directory

```bash
pip install -e .'[dev]'
```

Although most tutorials will show you the command without the single quotes around the `[dev]`, you will need them to make the `pip` command work in a zsh environment.

### isort
[isort](https://pycqa.github.io/isort/) is a tool for sorting your imports. Although your code will run, it makes you have your import statements always the same way which makes your code easier to read.

```shell
isort src/
```

### pylint
[pylint](https://pylint.readthedocs.io/en/stable/) is a linter for your python code. A linter does basic checks but can be handy so we included it in the project.

```shell
pylint src/
```

This will output something like

```text
------------------------------------
Your code has been rated at 10.00/10
```

when you wrote good code according to pylint

or

```text
************* Module MyProject
MyProject/__init__.py:1:0: C0103: Module name "MyProject" doesn't conform to snake_case naming style (invalid-name)
************* Module MyProject.script
MyProject/script.py:44:8: C0103: Constant name "loglevel" doesn't conform to UPPER_CASE naming style (invalid-name)
MyProject/script.py:48:8: C0103: Constant name "loglevel" doesn't conform to UPPER_CASE naming style (invalid-name)
MyProject/script.py:50:8: C0103: Constant name "loglevel" doesn't conform to UPPER_CASE naming style (invalid-name)
MyProject/script.py:52:8: C0103: Constant name "loglevel" doesn't conform to UPPER_CASE naming style (invalid-name)
MyProject/script.py:54:8: C0103: Constant name "loglevel" doesn't conform to UPPER_CASE naming style (invalid-name)

------------------------------------------------------------------
Your code has been rated at 8.00/10 (previous run: 7.33/10, +0.67)
```

Where the `C0103` in the output above was the mistake we made which is an error against naming style conventions.

### Black

[Black](https://black.readthedocs.io/) is the uncompromising code formatter. This means when you run it, it will alter your code formatting without asking you for permission to alter it. It doesn't break your code, but if your code was already broken, it will not fix it either.  To run black you, run it from the project directory and run

```shell
black src/
```

This will output something like

```text
All done! ✨ 🍰 ✨
3 files left unchanged.
```

Since we included `black` as part of the cookie cutter template it is only a matter of installing the optional dependencies. If you setup the pre-commit hooks black will be automatically executed when you commit.

### flake8

[Flake8](https://flake8.pycqa.org/en/latest/) is a linter for python code. It will check your code for errors against the style code guides called "PEP". To run flake8 you run it from the project directory and run

```shell
flake8 src/
```

This will output something like

```text
src/__init__.py:1:1: W391 blank line at end of file
src/demo.py:10:1: F401 'requests' imported but unused
src/demo.py:25:80: E501 line too long (80 > 79 characters)
```

Since we included `flake8` as part of the cookiecutter template it is only a matter of installing the optional dependencies.

### radon
[radon](https://radon.readthedocs.io/en/latest/) allows you to easily produce code metrics which can give you an indicator on the quality of your code and indirectly how *risky* your code is.

```shell
radon cc src/
```

This will give you output with ranking values.
| Ranking value | Risk |
| --------------|------|
| A | low - simple block |
| B | low - well structured and stable block |
| C | moderate - slightly complex block |
| D | more than moderate - more complex block |
| E | high - complex block, alarming |
| F | very high - error prone, unstable block |


### sphinx

We can generate documentation using `sphinx`. The default documentation language for Python projects is ReStructuredText (RST). It looks a little bit like MarkDown (MD) but is sufficiently different enough to scratch your head. This is why it is a good idea to have a quick look at https://www.devdungeon.com/content/restructuredtext-rst-tutorial-0#syntax-examples.

The docs are located in your project directory under `docs`.

Since we included `sphinx` as part of the cookiecutter template it is only a matter of installing the optional dependencies.

If you get `WARNING: toctree contains reference to document '<your rst file>' that doesn't have a title: no link will be generated` it means that the `"` around the title doesn't match the length of the title. ReStructuredText is pickier than MarkDown, it is a bit of getting used to.

The sphinx `config.py` has been configured by default to look for your python module "up" one directory.

The sphinx `config.py` has been configured by default to use the extensions `sphinx.ext.autodoc`, `sphinx.ext.coverage`, `sphinx.ext.napoleon`, `sphinx.ext.todo` and `sphinx.ext.viewcode`.

To generate your documentation form you [docstrings](https://peps.python.org/pep-0257/) you place yourself in your project's root directory and run
```shell
sphinx-apidoc -o docs/ src/
```

The `-o` determines where your generated ReStructured Text will be placed and the last parameter indicates where your code lives. `sphinx-apidoc` determines if code is a module or not based on the presence of the `__init__.py` file in the directory.

To include your API documentation you will only need to include the generated `modules.rst`. Since we attempt to generate documentation for all our code, we already included `modules.rst` in the `index.rst` file in `docs/`.

To generate the html you place yourself in the `docs` directory and run `make html`, this will place the complete website in `_build\html`. Don't forget to run `make clean` if you want to rebuild the docs after making changes to your project.

In case you want a different theme for your docs have a look at the [sphinx themes gallery](https://sphinx-themes.readthedocs.io/en/latest/). On this page you will find the instructions to install the theme to your virtual environment and what to configure in `docs/conf.py`.

### pre-commit
[pre-commit](https://pre-commit.com/) allows you to run a number of checks automatically when you commit to your local repository. By setting this up you will thus create a simple automated way for increasing your code quality.

We included `pre-commit` as part of the cookiecutter template, it is already installed in your development environment.

### build
[build](https://pypi.org/project/build/) allows us to create packages for distribution and installing them in our virtual environment. To start building your package you run from the project root

```shell
python -m build
```

This will generate the `.whl` as well as the `.tar.gz` in the `dist/` directory, it will create. The files are used for redistributing your packages.
Since we included `build` as part of the cookiecutter template it is only a matter of installing the optional dependencies.

#### Installing your own package in your virtual environment

To install your own package into your virtual environment you run from the project root

```shell
pip install dist/my_package.whl
```

The package name is defined in your `pyproject.toml` as well as the version.


#### Uploading to your repository of choice

# Running my code as a command

The `pyproject.toml` has been equipped with 

```text
[project.scripts]
{{cookiecutter.project_name}} = "{{cookiecutter.project_name}}.{{cookiecutter.python_main_script}}:main"
```

By default we will take the project name as command, but if it makes sense to alter it do so. For demo purposes we demonstrate with the name `yihaa` which then returns `hello world`.

As you can see in the code snippet above the command goes into the code directory, which also bares the name of the project, and is followed by your script that contains the function you want to call. In our default setup this is the main function. If you have a good reason to change all this go ahead, it's your code ;).

# Linting the Dockerfile

The Dockerfile can be lint with [hadolint](https://github.com/hadolint/hadolint). We currently ignore:

```
DL3009 info: Delete the apt-get lists after installing something
DL3008 warning: Pin versions in apt get install. Instead of `apt-get install <package>` use `apt-get install <package>=<version>`
```